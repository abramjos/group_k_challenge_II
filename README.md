# SSD Based ROI Detection and Colour Classification

## Requirements
Script is developed in python 3.5 but should work fine with any other python3 as well 
+ Opencv > 3.0
+ Keras==2.1
+ Tensorflow==1.4 (tensorflow_gpu==1.4 with CUDA 8 is suggested if CUDA GPU is available)
+ Numpy==1.14
+ glob2
+ Argparser


## Usage 
model.py is a class file; so it can be easily accessed for ROI detection and colour classification
    
 Can use the following for testing individual images:    
``` python model.py -i path/to/input_image.jpg -o path/to/output_image.jpg ```

+ `path/to/input_image.jpg`  - path to input image, not necessarily jpg 
+ `path/to/output_image.jpg` - path to output from detected image.
        Images should be FFMPEG compatible.
        
## Testing weight files in best_weight folder
  `test.py` can be used to make predictions using all weight files listed in the weight folder.

You can use: 
```python test.py``` 

`result` folder will be having all the detected images in corresponding weight folders
  

 