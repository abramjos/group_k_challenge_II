from math import ceil
import cv2
import numpy as np
import argparse

from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras.models import model_from_json,load_model
from ssd_encoder_decoder.ssd_output_decoder import decode_detections
from keras_loss_function.keras_ssd_loss import SSDLoss


class colour_detector():
    """
    Colour detection class module:
    """
    def __init__(self,weight='ssd7_epoch-41_loss-1.1996_val_loss-1.2449.h5'):
        """
        Class initialization/ model and weight loaded;

            # Initialization argument - Full path of weight file that has to be used;default best file is used  
            # You can find more weight files in best_weight folder 
            # Json model is used for loading the model
        """
        self.h,self.w = (300,400)
        self.json_model='model.json'
        self.weight_path=weight
        
        json_file = open(self.json_model, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
        # self.model = model_from_json(loaded_model_json,custom_objects={'AnchorBoxes': AnchorBoxes,'compute_loss': ssd_loss.compute_loss})
        self.model=load_model(self.weight_path,custom_objects={'AnchorBoxes': AnchorBoxes,'compute_loss': ssd_loss.compute_loss})

    def _preprocess_img(self,image):
        """
        Image preprocessing to fit in the model;

            # Rotates the image if required(for landscape/portrait images)
            # Image will be resize to (300,400,3) to fit in the model(interpolation ensures least loss of data)
        """
        
        if image.shape[0] > image.shape[1]:
            image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
        h,w = image.shape[:2]
        hr = ceil(h/self.h)
        wr = ceil(w/self.w)
        image_resize = cv2.resize(image,(int(w/wr),int(h/hr)),interpolation = cv2.INTER_AREA)
        image_resize = cv2.resize(image_resize,(self.w,self.h),interpolation = cv2.INTER_CUBIC)
        return(image_resize)

    def _get_colour(self,im_roi):
        """
        Colour extractor from the ROI developed by model;
            # from image ROI, the roi corresponding to pink/yellow are extracted(with H&S)
            # based on threshhold decision is made :: No explicit detection for blue
        """
        
        image=im_roi
        h,w=image.shape[:2]

        hsv_roi=cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
        v=hsv_roi[:,:,2]
        v_filter=cv2.inRange(v,120,255)
        image_roi=cv2.cvtColor(hsv_roi,cv2.COLOR_HSV2BGR)
        roi_masked=cv2.bitwise_and(image_roi,image_roi,mask = v_filter)
        hsv_final=cv2.cvtColor(roi_masked,cv2.COLOR_BGR2HSV)
        
        #Red content in ROI is extraced which is unsaturated
        red_upper=cv2.inRange(hsv_final[:,:,0],0,10)
        red_lower=cv2.inRange(hsv_final[:,:,0],140,180)
        red_unsat=cv2.inRange(hsv_final[:,:,1],100,225)
        red_unsaturated=cv2.bitwise_and(roi_masked,roi_masked,mask = red_unsat)
        red=red_upper+red_lower
        roi_red=cv2.bitwise_and(red_unsaturated,red_unsaturated,mask = red)
        red_size=np.count_nonzero(roi_red)

        #Blue content in ROI is extraced which is unsaturated
        yellow=cv2.inRange(hsv_final[:,:,0],15,45)
        yellow_unsat=cv2.inRange(hsv_final[:,:,1],150,225)
        yellow_unsaturated=cv2.bitwise_and(roi_masked,roi_masked,mask = yellow_unsat)
        roi_yellow=cv2.bitwise_and(yellow_unsaturated,yellow_unsaturated,mask = yellow)
        yellow_size=np.count_nonzero(roi_yellow)

        #Using thresholding, determines the colour of roi        
        threshold=int(image.size/100)
        if red_size>threshold or yellow_size>threshold:
            if red_size>yellow_size:
                out=([200,0,255],'pink')
            else:
                out=([0,225,225],'yellow')
        else:
            out=([255,0,0],'blue')
        return(out)


    def predict(self,image):
        """
        Predict function for colour_detector()
            # Input : image    -  input image argument          (uint8)
            # Output: bgr      -  bgr array of colour           (np.array)
                      colour   -  detected colour               (str)
                      out_image-  colour strip with input_image (uint8)

        Batch detection is also possible instead of single imag detection
        
        """
        try:
            
            im_resize=self._preprocess_img(image)
            im_batch=np.reshape(im_resize,(1,300, 400, 3))
            
            pred = self.model.predict(im_batch)
            box_pred_decoded = decode_detections(pred,
                                               confidence_thresh=0.5,iou_threshold=0.45,
                                               top_k=200,normalize_coords=True,
                                               img_height=300,img_width=400)

            if box_pred_decoded[0].shape==(0,):
                print("\n\nNo Object Detection Occured")
                return(False)
            else:
                confidence=box_pred_decoded[0][0][1]
                box=abs(box_pred_decoded[0][0].astype('int'))            
                
                roi_image=im_resize[box[2]:box[4],box[3]:box[5],:]

                bgr,colour=self._get_colour(roi_image)
                frame=np.ones((20,image.shape[1],3))        
                strip=cv2.rectangle(frame,(1,1),(image.shape[1],20),bgr,thickness=cv2.FILLED)
                out_image=np.vstack([image,strip])       
                return(bgr,colour,out_image)


        except AttributeError:
            print("Argument is uint8 image")
            return(False)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--input_image',type=str,default=False,
                        help='Image input for testing')
    parser.add_argument('-o','--output_image',type=str,default='out.jpg',
                        help='Output images')
    args = parser.parse_args()

    detect=colour_detector()

    image=cv2.imread(args.input_image)
    print('::::::::::: ROI COLOR DETECTOR :::::::::::')
    print('\n\n\tInput image : %s'%args.input_image)

    try:
        bgr,colour,out_image=detect.predict(image)
        cv2.imshow('OUTPUT',out_image)
        print('\n\n\tColor is R-{}\tG-{}\tR-{}'.format(bgr[2],bgr[1],bgr[0]))
        print('\n\tOutput image saved as : %s\n\n'%args.output_image)
        cv2.imwrite(args.output_image,out_image)

    except:
        print("No ROI found")