import unittest
from model import colour_detector
import cv2
import numpy as np 

class colour_detector(unittest.TestCase):

	def setUp(self):
		self.detect=colour_detector()

	def test_preprocess(self):
		test=np.zeros((300,400,3)).astype(np.uint8)
		for i in range(np.random.randint(10)):
			height,width=np.random.randint(300,5000),np.random.randint(400,5000)
			image=np.zeros((height,width,3)).astype(np.uint8)
			import ipdb;ipdb.set_trace()
			self.detect._preprocess_img.assertAlmostEqual(test,image)


	def test_predict(self):

		self.assertRaises(ValueError,self.detect.predict,None)

		red=cv2.imread('red.jpg')
		yellow=cv2.imread('yellow.jpg')
		blue=cv2.imread('blue.jpg')

		self.assertEqual(self.detect.predict(red),'red')
		self.assertEqual(self.detect.predict(yellow),'yellow')
		self.assertEqual(self.detect.predict(blue),'blue')

	def tearDown(self):
		pass

if __name__ == '__main__':
	unittest.main()
