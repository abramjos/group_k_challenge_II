from model import colour_detector #calling class 
import glob
import cv2
import numpy as np
import os

'''
 Sample code to check the best weight values saved in the result/ folder  
'''
lst=glob.glob('input&out/data_in/*')
weight = glob.glob('best_weights/*')

#creating folder result
try:
    os.stat('result')
except:
    os.mkdir('result')

for model_weight in weight:

    detect=colour_detector(model_weight) #class object detect is created

    folder=model_weight.split('.')[0].replace('best_weights','result')
    
    #creating folder for each weights in result folder
    try:
        os.stat(folder)
    except:
        os.mkdir(folder)

    for i in lst:
        try:
            im=cv2.imread(i)
            _,colour,out=detect.predict(im)
            print("\n\n Detected colour is {} for image {}".format(colour,i))
            name=i.replace('input&out/data_in',folder)
            cv2.imwrite(name,out)
            
        except:
            print('error')
